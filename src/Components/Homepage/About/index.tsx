import React from "react"
import { Typography } from "@mui/material"
import arrow from '../../../Public/img/arrow.svg'
import './index.scss'
import { Box } from "@mui/system"

const About = () => {
  return (
    <Box sx={{ display: 'flex', flexDirection: 'row', height: 450, padding: '0 2rem 0rem 0rem' }}>
      <Box sx={{ flex: 1, display: 'flex', flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'center' }}>
        <Typography variant="h2" fontWeight={600}>Why</Typography>
        <Typography variant="h6" fontWeight={600}>Manchester Design Formula</Typography>
        <Typography variant="body1" align="left">Lorem ipsum dolor sit amet consectetur adipisicing elit. Et odit, iusto ipsam ratione minus veniam nisi provident doloremque dolore distinctio!</Typography>
        <Typography variant="body1" align="left">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sit asperiores, nisi optio molestias qui quam aut illo quaerat, neque ipsa </Typography>
        <Typography variant="subtitle1">READ MORE</Typography>
      </Box>
      <Box sx={{
        flex: 1, display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start',
        backgroundImage: `url(${arrow})`,
        backgroundPosition: 'center',
        backgroundSize: '300px',
        backgroundRepeat: 'no-repeat'
      }}>
        <iframe src='https://www.youtube.com/embed/E7wJTI-1dvQ'
          frameBorder={0}
          allow='autoplay; encrypted-media'
          allowFullScreen
          title='video'
          height={200}
          width={350}
        />
      </Box>
    </Box >

  )
}

export default About
