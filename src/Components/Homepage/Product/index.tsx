import React from "react"
import { Paper, Typography } from "@mui/material"
const Product = (props: any) => {
  return (
    <Paper sx={{ height: 450 }} elevation={1}>
      <img src={props.item.image} className="bedsheet-image" alt="pop" />
      {props.item.name && <Typography> {props.item.name}</Typography>}
      {props.item.description && <Typography> {props.item.description}</Typography>}
      {props.item.originalPrice && <Typography> {props.item.originalPrice}</Typography>}
    </Paper >
  )
}

export default Product
