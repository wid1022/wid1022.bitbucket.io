import React from "react"
import About from "./About"
import Dashboard from "./Dashboard"
import Footer from "./Footer/index"
import Navigation from './Navbar/index'
import { Box } from "@mui/system"
import Copyright from "./Copyright"
import Products from './Products/index'
import Moreproducts from "./Moreproducts"

const Homepage = () => {
  return (
    <Box>
      <Navigation />
      <Box sx={{ padding: "0 4rem", margin: 0 }}>
        <Dashboard />
        <About />
        <Products />
        <Moreproducts />
        <Footer />
      </Box>
      <Copyright />
    </Box>
  )
}

export default Homepage
