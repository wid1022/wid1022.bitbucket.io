import React from "react"
import Product from '../Product/index'
import { Box } from "@mui/system"
import { Typography } from "@mui/material"
const Moreproducts = () => {
  var products = [
    {
      name: "Everyday Basics - Rose Printed",
      image: "https://picsum.photos/300/350"
    },
    {
      name: "Everyday Basics - Rose Printed",
      image: "https://picsum.photos/300/350"
    },
    {
      name: "Everyday Basics - Rose Printed",
      image: "https://picsum.photos/300/350"
    },
    {
      name: "Everyday Basics - Rose Printed",
      image: "https://picsum.photos/300/350"
    }
  ]
  return (
    <Box sx={{ height: 550 }}>
      <Typography variant="h2" fontWeight={600} align="left">More Products</Typography>
      <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', margin: '.5rem 0' }}>
        {products.map((product, i) => <Product key={i} item={product} />)}
      </Box>
    </Box>
  )
}

export default Moreproducts
