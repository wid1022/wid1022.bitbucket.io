import React from "react"
import { Grid, Typography } from "@mui/material"
import { Box } from "@mui/system"
import Button from '../../Button/index'
import './index.scss'

const Item = (props: any) => {
  return (
    <Grid container direction="row" alignItems="center">
      <Grid item md={5} zIndex={100}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: "flex-start",
            height: 450,
          }}
        >
          <Typography variant='h1' sx={{ fontWeight: 600 }}>{props.item.name1}</Typography>
          <Typography variant='h1' sx={{ fontWeight: 600 }}>{props.item.name2}</Typography>
          <Typography variant='body1' align="left">{props.item.description}</Typography>
          <Box sx={{ margin: "2rem 0" }}>
            <Button text="SHOP NOW" />
          </Box>
        </Box>
      </Grid>
      <Grid item md={7} >
        <img src={props.item.image} alt="" className="dashboard-img" />
      </Grid>
    </Grid>
  )
}

export default Item
