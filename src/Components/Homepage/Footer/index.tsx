import { Typography } from "@mui/material"
import { Box } from "@mui/system"
import React from "react"
import Button from '../Button/index'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faFacebook, faInstagram, faApple, faPinterest, faTwitter, faYoutube } from "@fortawesome/free-brands-svg-icons"
// fontawesome.library.add(faCoffee, faCheckSquare)

const Footer = () => {
    return (
        <Box sx={{ display: 'flex', flexDirection: "row", height: 250, padding: '2rem 0', borderTop: '1px solid lightgrey' }}>
            <Box sx={{ flex: 7, display: 'flex', flexDirection: 'row', justifyContent: 'space-around' }}>
                <Box sx={{ display: 'flex', flexDirection: 'column', flex: 1, justifyContent: "space-around" }}>

                    <Box sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }} >
                        <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around' }}>
                            <FontAwesomeIcon icon={faFacebook}></FontAwesomeIcon>
                            <FontAwesomeIcon icon={faTwitter}></FontAwesomeIcon>
                            <FontAwesomeIcon icon={faYoutube}></FontAwesomeIcon>
                        </Box>
                        <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around' }}>
                            <FontAwesomeIcon icon={faApple}></FontAwesomeIcon>
                            <FontAwesomeIcon icon={faPinterest}></FontAwesomeIcon>
                            <FontAwesomeIcon icon={faInstagram}></FontAwesomeIcon>
                        </Box>
                    </Box>
                </Box>
                <Box sx={{ flex: 1, display: 'flex', flexDirection: 'column', justifyContent: 'space-around', alignItems: "flex-start" }}>
                    <Typography variant="h6" sx={{ fontWeight: 600 }}>ABOUT US</Typography>
                    <Typography variant="body2">Our Promise</Typography>
                    <Typography variant="body2">Blog</Typography>
                    <Typography variant="body2">In News</Typography>
                    <Typography variant="body2">Contact Us</Typography>
                </Box>
                <Box sx={{ flex: 1, display: 'flex', flexDirection: 'column', justifyContent: 'space-around', alignItems: 'flex-start' }}>
                    <Typography variant="h6" sx={{ fontWeight: 600 }}>ABOUT US</Typography>
                    <Typography variant="body2" >Our Promise</Typography>
                    <Typography variant="body2">Blog</Typography>
                    <Typography variant="body2">In News</Typography>
                    <Typography variant="body2">Contact Us</Typography>
                </Box>
                <Box sx={{ flex: 1, padding: '2rem 0', display: 'flex', flexDirection: 'column', justifyContent: 'space-around', alignItems: 'flex-start' }}>
                    <Typography variant="body2">Our Promise</Typography>
                    <Typography variant="body2">Blog</Typography>
                    <Typography variant="body2">In News</Typography>
                </Box>
            </Box>
            <Box sx={{ flex: 5, display: 'flex', flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'center' }}>
                <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
                    <Typography variant="h6" sx={{ fontWeight: 600 }}>Excited For Interesting Updates?</Typography>
                    <Typography variant="body1" align="left">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptates expedita sequi accusamus labore recusandae nesciunt, itaque facilis adipisci dolore hic debitis aspernatur, quo culpa laudantium similique, maiores voluptate error vel?</Typography>
                </Box>
                <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', padding: '.75rem 0' }}>
                    <Box >
                        <input type="text" placeholder="Enter Email Address" style={{ padding: '0 1.25rem', height: '40px', width: 300 }} />
                    </Box>
                    <Box sx={{ margin: '0 1rem' }}>
                        <Button text="SIGN UP" />
                    </Box>
                </Box>
            </Box>

        </Box >
    )
}

export default Footer
